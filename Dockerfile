FROM python:alpine

EXPOSE 8000

WORKDIR /app
COPY . .

ENTRYPOINT ["python3", "webserver.py"]
